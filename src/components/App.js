import React, { Component } from 'react'
import config from '../config'
import ReactPaginate from 'react-paginate'
import ArticleList from './ArticleList'
class App extends Component {
  state = {
    page: 1,
    offset: 0,
    pages: 0,
    notifications: []
  }
  componentDidMount () {
    this.getData()
  }
  getData = async (page = this.state.page) => {
    try {
      const data = await fetch(`${config.url}?page=${page}`,
        {
          headers: {
            Authorization: `Bearer ${config.token}`
          },
          mode: 'cors'
        })
      const {response: {notifications, count}} = await data.json()
      const pages = await Math.ceil(+count / 20)
      await this.setState((prevState) => {
        return {pages, page: page, notifications}
      })
    } catch (err) {
      console.log(err)
    }
  }
  handlePageClick = (data) => {
    let selected = data.selected + 1
    this.getData(selected)
  }

  render () {
    const date = (new Date()).toString().split(' ').splice(1, 3).join(' ')
    return (
      <div className="container">
        <ul className="timeline">
          <li className="time-label">
            <span className="bg-red">
              {date}
            </span>
          </li>
          <ArticleList data={this.state.notifications}/>
          <li>
            <i className="fa fa-clock bg-gray"></i>
          </li>
        </ul>
        <ReactPaginate
          previousLabel="&#8249;"
          nextLabel="&#8250;"
          breakLabel={<span>...</span>}
          breakClassName="break-me"
          pageCount={this.state.pages}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={this.handlePageClick}
          containerClassName="pagination"
          subContainerClassName="pages pagination"
          activeClassName="active" />
      </div>
    )
  }
}

export default App
