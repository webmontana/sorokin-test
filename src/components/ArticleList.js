import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'

function ArticleList ({data}) {
  let commentNodes = data.map(({text, created_at, id}) => {
    moment.locale('ru')
    const date = moment(created_at).format('LLL')
    return (
      <li key={id} dangerouslySetInnerHTML={{__html: `
      <i class="fa fa-flag bg-green"></i>
        <div class="timeline-item">
          <span class="time"><i class="far fa-clock"></i> ${date}</span>
          <div class="timeline-body">
            ${text}
          </div>
        </div>
      `}}/>
    )
  })
  return commentNodes
}
ArticleList.propTypes = {
  data: PropTypes.array
}
ArticleList.defaultProps = {
  data: []
}

export default ArticleList
